# Boot Animation
ifneq ($(wildcard vendor/aoscp/prebuilt/bootanimation/$(TARGET_SCREEN_WIDTH)x$(TARGET_SCREEN_HEIGHT).zip),)
PRODUCT_COPY_FILES += \
    vendor/aoscp/prebuilt/bootanimation/$(TARGET_SCREEN_WIDTH)x$(TARGET_SCREEN_HEIGHT).zip:system/media/bootanimation.zip
endif
